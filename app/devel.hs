{-# LANGUAGE PackageImports #-}
import Application(app)
import Prelude (IO)

main :: IO ()
main = app
